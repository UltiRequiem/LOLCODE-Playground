# LOLCODE Examples
Various LOLCODE examples.

### Installing
I highly recommend not installing this. Just go to [Repl.it](https://repl.it/new/lolcode) and work there.

### Executing program

* Click run in Repl.it
![Screenshot](https://i.imgur.com/ctcITX5.png)
## Help
[![Gmail Badge](https://img.shields.io/badge/-eliaz.bobadilladev@gmail.com-c14438?style=flat&logo=Gmail&logoColor=white&link=mailto:eliaz.bobadilladev@gmail.com)](mailto:eliaz.bobadilladev@gmail.com)  [![Twitter Badge](https://img.shields.io/badge/-@EliazBobadilla-00acee?style=flat&logo=twitter&logoColor=white&link=https://twitter.com/@EliazBobadilla/)](https://www.twitter.com/@EliazBobadilla/) 
